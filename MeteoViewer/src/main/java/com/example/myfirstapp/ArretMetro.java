package com.example.myfirstapp;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ArretMetro {

    private String id;

    private String name;

    private double lon;

    private double lat;

    private String zone;

    private JSONArray linesJson;

    private List<String> lines = new ArrayList<>();

    public List<String> getLines() {
        return lines;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }


    public ArretMetro(JSONObject object) throws JSONException {
        name = object.getString("name");
        id = object.getString("id");
        lon = object.getDouble("lon");
        lat = object.getDouble("lat");
        zone = object.getString("zone");
        linesJson = object.getJSONArray("lines");

        for (int i = 0; i < linesJson.length(); i++) {
            lines.add(linesJson.getString(i));
        }
    }

    public ArretMetro(String name, String id) {
        this.name = name;
        this.id = id;
    }


}
