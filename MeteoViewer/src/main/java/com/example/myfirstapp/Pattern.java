package com.example.myfirstapp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Pattern {

    private String id;
    private String desc;
    private int dir;
    private String shortDesc;

    private List<Times> times;

    public List<Times> getTimes() {
        return times;
    }

    public void setTimes(List<Times> times) {
        this.times = times;
    }

    public Pattern(JSONObject object) throws JSONException {
        JSONObject jsonPattern = object.getJSONObject("pattern");
        this.id = jsonPattern.getString("id");
        this.desc = jsonPattern.getString("desc");
        this.dir = jsonPattern.getInt("dir");
        this.shortDesc = jsonPattern.getString("shortDesc");
        times = new ArrayList<>();

        JSONArray jsonTimes = object.getJSONArray("times");

        for (int i=0; i<jsonTimes.length(); i++) {
            times.add(new Times(jsonTimes.getJSONObject(i)));
        }

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getDir() {
        return dir;
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    @Override
    public String toString() {
        String id2="";
        if (id!= null) {
            String[] res = id.split(":");
            id2 = res[1];
        }
        return id2+" | "+getDesc();
    }
}
