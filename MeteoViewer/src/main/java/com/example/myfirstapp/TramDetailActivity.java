package com.example.myfirstapp;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TramDetailActivity extends AppCompatActivity {

    private Spinner sensSpinner;
    private ListView timeLstView;
    private TextView txtName;

    private Patterns patterns;
    private String[] ids;
    private String nameArret;

    List<Pattern> spinnerStringList = new ArrayList<>();
    PatternAdapter spinnerAdapter;

    FloatingActionButton floatingActionButton;
    TimeAdapter timeAdapter;

    SharedPreferences favorisArrets;
    Boolean isFavoris = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tram_detail);
        ids = getIntent().getStringArrayExtra("idList");
        txtName = findViewById(R.id.txtName);
        nameArret = getIntent().getStringExtra("nameArret");
        txtName.setText(nameArret);
        sensSpinner = findViewById(R.id.spinnerSens);
        spinnerAdapter = new PatternAdapter(this, android.R.layout.simple_list_item_1);
        floatingActionButton = findViewById(R.id.star);
        favorisArrets = getSharedPreferences("favoris", MODE_PRIVATE);
        isFavoris = isFav(favorisArrets.getString("favoris", ""));
        if (isFavoris) {
            floatingActionButton.setImageDrawable(getDrawable(R.drawable.ic_star));
        } else {
            floatingActionButton.setImageDrawable(getDrawable(R.drawable.ic_star_border));
        }

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFavoris) {
                    isFavoris=false;
                    String favoris = favorisArrets.getString("favoris", "");
                    String[] fav = favoris.split("&");

                    String favNew = "";
                    for (int i=0; i<fav.length; i++) {
                        String[] favId = fav[i].split("#");
                        if (favId.length>0) {
                            if (!favId[0].replaceAll(" ","").equals(nameArret.replaceAll(" ",""))) {

                                for (int j=0;j<favId.length;j++) {
                                    if (j==favId.length-1){
                                        favNew+=favId[j];
                                    } else {
                                        favNew+=favId[j]+"#";
                                    }

                                }
                                favNew+="&";
                            }
                        }
                    }
                    favorisArrets.edit().putString("favoris",favNew).commit();
                    floatingActionButton.setImageDrawable(getDrawable(R.drawable.ic_star_border));
                    Toast toast = Toast.makeText(getApplicationContext(), "Retirer des favoris !", Toast.LENGTH_SHORT);
                    toast.show();

                } else {
                    String favNow = favorisArrets.getString("favoris", "");
                    String newFav =nameArret;
                    for (int i=0; i<ids.length;i++){
                        newFav = newFav + "#" + ids[i];
                    }
                    favorisArrets.edit().putString("favoris",favNow+"&"+newFav).commit();
                    floatingActionButton.setImageDrawable(getDrawable(R.drawable.ic_star));
                    isFavoris = true;
                    Toast toast = Toast.makeText(getApplicationContext(), "Ajouter des favoris !", Toast.LENGTH_SHORT);
                    toast.show();
                }

            }
        });

        sensSpinner.setAdapter(spinnerAdapter);
        sensSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Pattern pattern = spinnerAdapter.getItem(position);
                timeAdapter.addAll(pattern.getTimes());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        patterns = new Patterns();

        timeLstView = findViewById(R.id.lstLines);
        timeAdapter = new TimeAdapter(this, android.R.layout.simple_list_item_1);
        timeLstView.setAdapter(timeAdapter);
        new PatternsFinder().execute();
    }

    private boolean isFav(String favoris) {
        String[] fav = favoris.split("&");
        for (int i=0; i<fav.length; i++) {
            String[] favId = fav[i].split("#");
            if (favId.length>0) {
                if (favId[0].replaceAll(" ","").equals(this.nameArret.replaceAll(" ",""))) {
                    return true;
                }
            }
        }
        return false;
    }






    private class PatternsFinder extends AsyncTask<String, Boolean, Boolean> {
        protected void onPreExecute() {

        }

        protected Boolean doInBackground(String... urls ) {

            for(int i=0; i<ids.length; i++){
                callWebService(ids[i]);
            }
            onPostExecute(1);
            return true;
        }

        protected void onPostExecute(Integer result) {

        }



        private void callWebService(String id) {
            String url ="http://data.metromobilite.fr/api/routers/default/index/stops/"+id+"/stoptimes";
            RequestQueue queue = Volley.newRequestQueue(TramDetailActivity.this);

            StringRequest request = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            String a = response;
                            patterns.ajouterPatterns(a);
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    spinnerStringList.clear();
                                    spinnerAdapter.addAll(patterns.getPatterns());

                                }

                            });


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyError a = error;
                        }
                    });

            queue.add(request);
        }


    }

}
