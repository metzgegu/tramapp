package com.example.myfirstapp;

import org.json.JSONException;
import org.json.JSONObject;

public class Times {

    private String stopId;
    private String stopName;
    private int scheduledArrival;
    private int scheduledDeparture;
    private int realtimeArrival;
    private int realtimeDeparture;
    private int arrivalDelay;
    private int departureDelay;
    private boolean timePoint;
    private boolean realTime;
    private int tripid;



    public Times(JSONObject jsonObject) throws JSONException {
        this.stopId = jsonObject.getString("stopId");
        this.stopName = jsonObject.getString("stopName");
        this.scheduledArrival = jsonObject.getInt("scheduledArrival");
        this.scheduledDeparture = jsonObject.getInt("scheduledDeparture");
        this.realtimeArrival = jsonObject.getInt("realtimeArrival");
        this.realtimeDeparture = jsonObject.getInt("realtimeDeparture");
        this.arrivalDelay = jsonObject.getInt("arrivalDelay");
        this.departureDelay = jsonObject.getInt("departureDelay");
        this.timePoint = jsonObject.getBoolean("timepoint");
        this.realTime = jsonObject.getBoolean("realtime");
        this.tripid = jsonObject.getInt("tripId");



    }

    public String getStopId() {
        return stopId;
    }

    public String getStopName() {
        return stopName;
    }

    public int getScheduledArrival() {
        return scheduledArrival;
    }

    public int getScheduledDeparture() {
        return scheduledDeparture;
    }

    public int getRealtimeArrival() {
        return realtimeArrival;
    }

    public int getRealtimeDeparture() {
        return realtimeDeparture;
    }

    public int getArrivalDelay() {
        return arrivalDelay;
    }

    public int getDepartureDelay() {
        return departureDelay;
    }

    public boolean isTimePoint() {
        return timePoint;
    }

    public boolean isRealTime() {
        return realTime;
    }

    public int getTripid() {
        return tripid;
    }

    public String getTime() {
        return timeConversion(realtimeArrival);
    }


    private static String timeConversion(int totalSeconds) {
        int hours = totalSeconds / 60 / 60;
        int minutes = (totalSeconds - (hoursToSeconds(hours))) / 60;
        if (hours >23) {
            hours = hours - 24;
        }
        String strHours = hours+"";
        String strMinutes = minutes+"";
        if(strHours.length() < 2) {
            strHours = "0" + strHours;
        }
        if (strMinutes.length() <2) {
            strMinutes = "0" + strMinutes;
        }
        return strHours + "h" + strMinutes;
    }

    private static int hoursToSeconds(int hours) {
        return hours * 60 * 60;
    }

    private static int minutesToSeconds(int minutes) {
        return minutes * 60;
    }
}
