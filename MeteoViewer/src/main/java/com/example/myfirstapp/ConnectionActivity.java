package com.example.myfirstapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ConnectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TextView textView = (TextView) findViewById(R.id.textView);
        final Button submit = (Button) findViewById(R.id.submit);
        final EditText mdp = (EditText) findViewById(R.id.mdp);
        final EditText login = (EditText) findViewById(R.id.login);
        final Context c = this;

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(c, HomeActivity.class);
                intent.putExtra("userLogin", login.getText());
                startActivity(intent);
            }
        });


    }

    private void getData() {


    }
}
