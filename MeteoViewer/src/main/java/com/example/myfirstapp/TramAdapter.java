package com.example.myfirstapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TramAdapter extends ArrayAdapter<ArretsCoupleMetro> {
    Context m_context;
    List<ArretsCoupleMetro> lstItems;
    LayoutInflater inflter;

    public TramAdapter(@NonNull Context context, int ressource) {
        super(context,ressource);

        m_context = context;
        lstItems = new ArrayList<>();
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getPosition(ArretsCoupleMetro arretMetro) {
        return lstItems.indexOf(arretMetro);
    }

    @Override
    public int getCount() {
        return lstItems.size();
    }

    @Override
    public ArretsCoupleMetro getItem(int position){
        return lstItems.get(position);
    }

    @Override
    public void addAll(@NonNull Collection<? extends ArretsCoupleMetro> collection) {
        lstItems.clear();
        lstItems.addAll(collection);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
         convertView = inflter.inflate(R.layout.listview_item, null);
        TextView txtName = convertView.findViewById(R.id.txtName);

        ArretsCoupleMetro arretMetro = getItem(position);

        txtName.setText(arretMetro.getName());

        return convertView;
    }
}
