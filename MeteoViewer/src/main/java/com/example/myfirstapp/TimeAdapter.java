package com.example.myfirstapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TimeAdapter  extends ArrayAdapter<Times> {

    List<Times> lstItems;
    LayoutInflater inflter;

    public TimeAdapter(@NonNull Context context, int resource) {
        super(context, resource);
        lstItems = new ArrayList<>();
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getPosition(Times time) {
        return lstItems.indexOf(time);
    }

    @Override
    public int getCount() {
        return lstItems.size();
    }

    @Override
    public Times getItem(int position){
        return lstItems.get(position);
    }

    @Override
    public void addAll(@NonNull Collection<? extends Times> collection) {
        lstItems.clear();
        lstItems.addAll(collection);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.time_layout, null);
        TextView txtName = convertView.findViewById(R.id.txtTime);

        Times time = getItem(position);
        txtName.setText(time.getTime());

        return convertView;
    }
}
