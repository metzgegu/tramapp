package com.example.myfirstapp;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ArretsMetro {

    private static final String[] trams = {"SEM:A", "SEM:B", "SEM:C", "SEM:D", "SEM:E"};

    private List<ArretsCoupleMetro> lesArrets;

    private String jsonString;

    public List<ArretsCoupleMetro> getLesArrets(){
        return lesArrets;
    }

    public ArretsMetro(List<ArretsCoupleMetro> arrets) {
        lesArrets = arrets;
    }

    public ArretsMetro(String jsonString) {
        try {
            lesArrets = new ArrayList<>();

            this.jsonString = jsonString;

            JSONArray array = new JSONArray(jsonString);

            for (int i = 0; i < array.length(); i++) {
                ArretMetro arretMetro = new ArretMetro(array.getJSONObject(i));
                if (arretMetro.getLines() != null){
                    if (arretMetro.getLines().size() != 0){
                        String test = arretMetro.getLines().get(0);
                        for (String s: trams){
                            if (s.equals(test)){
                                // tramArrets.add(arretMetro);
                                boolean alreadyexist = false;
                                for(ArretsCoupleMetro arretsCoupleMetro: lesArrets) {
                                    if (arretsCoupleMetro.contains(arretMetro)) {
                                        arretsCoupleMetro.addArret(arretMetro);
                                        alreadyexist = true;
                                    }
                                }
                                if (!alreadyexist) {
                                    ArretsCoupleMetro arretCoupleMetro = new ArretsCoupleMetro(arretMetro);
                                    lesArrets.add(arretCoupleMetro);
                                }
                                break;
                            }
                        }
                    }
                }

            }



        } catch (Exception e){
            String err = e.getMessage();
        }
    }


}
