package com.example.myfirstapp;

import java.util.ArrayList;
import java.util.List;

public class ArretsCoupleMetro {

    private List<ArretMetro> arrets;
    private String name;


    public String id;

    public void addArret(ArretMetro arretMetro) {
        arrets.add(arretMetro);
    }

    public String getName() {
        String[] tab = this.name.split(",");
        String newName = this.name;
        if (tab.length > 1) {
            newName ="";
            for (int i = 1; i < tab.length; i++) {
                newName += tab[i]+" ";
            }
        }
        return newName;
    }

    public String[] getListOfId() {
        //List<String> res = new ArrayList<>();
        String[] res = new String[arrets.size()];
        int i=0;
        for (ArretMetro arretMetro: arrets) {
            //res.add(arretMetro.getId());
            res[i] = arretMetro.getId();
            i++;
        }
        return res;
    }

    public ArretsCoupleMetro(ArretMetro arretMetro){
        arrets = new ArrayList<>();
        arrets.add(arretMetro);
        name = arretMetro.getName();
        id = arretMetro.getId();
    }

    public boolean contains(ArretMetro arretMetro) {
        boolean res = false;
        for (ArretMetro am: arrets) {
            if (arretMetro.getName().equals(am.getName())){
                res = true;
                break;
            }
        }
        return res;
    }
}
