package com.example.myfirstapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;


public class ListviewActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private DrawerLayout drawerLayout;
    private NavigationView navigationMenu;
    private boolean menuOpen;

    double latitude;
    double longitude;
    int kmRayon;

    ListView arretsList;
    SwipeRefreshLayout swipeRefreshLayout;
    SeekBar sbKm;
    TextView tvKm;


    List<ArretsCoupleMetro> libellesArrets = new ArrayList<>();
    TramAdapter arretsAdapter;

    ArretsMetro arretsMetro = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_home);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        navigationMenu = findViewById(R.id.nav_view);

        navigationMenu.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                return manageNavigationViewClick(menuItem);
            }
        });

        View viewHeader = getLayoutInflater().inflate(R.layout.nav_header, null);

        navigationMenu.addHeaderView(viewHeader);

        arretsList = findViewById(R.id.arretsList);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);

        swipeRefreshLayout.setOnRefreshListener(this);


        arretsAdapter = new TramAdapter(this, android.R.layout.simple_list_item_1);
        kmRayon=1000;
        tvKm = findViewById(R.id.textkm);
        sbKm = findViewById(R.id.sbKm);
        tvKm.setText("1000 m");
        sbKm.setProgress(kmRayon);
        sbKm.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            // When Progress value changed.
            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                progress = progressValue;

                tvKm.setText(progressValue+" m");
                kmRayon = progressValue;
                new ArretProcheLocation().execute("");
            }

            // Notification that the user has started a touch gesture.
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
               // Toast.makeText(getApplicationContext(), "Started tracking seekbar", Toast.LENGTH_SHORT).show();
            }

            // Notification that the user has finished a touch gesture
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                tvKm.setText(progress+" m");
                kmRayon = progress;
            }
        });

        arretsList.setAdapter(arretsAdapter);
        if (!checkPermissionForLocation()) {
            requestPermissionForLocation();
        } else {
            _getLocation();

            new ArretProcheLocation().execute("");
        }

        arretsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                ArretsCoupleMetro o = (ArretsCoupleMetro)arretsList.getItemAtPosition(position);
                Intent intent = new Intent(getBaseContext(), TramDetailActivity.class);
                intent.putExtra("idList", o.getListOfId());
                intent.putExtra("nameArret", o.getName());
                startActivity(intent);
            }
        });



    }


    /**
     * Recupere la location x et y de l'apareil
     */
    private void _getLocation() {

        long m= 1000;
        float meter = 50;

        try {
            LocationManager  locationManager = (LocationManager) this
                    .getSystemService(LOCATION_SERVICE);
            Location location= null;
            // getting GPS status
            boolean isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            boolean isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            boolean canGetLocation = true;
            if (isNetworkEnabled) {


                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        m,
                        meter, new LocationListener() {
                            @Override
                            public void onLocationChanged(Location location) {
                            }

                            @Override
                            public void onStatusChanged(String provider, int status, Bundle extras) {
                            }

                            @Override
                            public void onProviderEnabled(String provider) {
                            }

                            @Override
                            public void onProviderDisabled(String provider) {
                            }
                        });
                Log.d("Network", "Network Enabled");
                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null) {
                         latitude = location.getLatitude();
                         longitude = location.getLongitude();
                    }
                }
            }
            // if GPS Enabled get lat/long using GPS Services
            if (isGPSEnabled) {
                if (location == null) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            m,
                            meter, new LocationListener() {
                                @Override
                                public void onLocationChanged(Location location) {

                                }

                                @Override
                                public void onStatusChanged(String provider, int status, Bundle extras) {

                                }

                                @Override
                                public void onProviderEnabled(String provider) {

                                }

                                @Override
                                public void onProviderDisabled(String provider) {

                                }
                            });
                    Log.d("GPS", "GPS Enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
            }
        } catch (SecurityException e ) {
            e.printStackTrace();
        }


    }

    @Override
    public void onRefresh() {
        _getLocation();

        new ArretProcheLocation().execute("");
    }


    public boolean checkPermissionForLocation() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public void requestPermissionForLocation() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            Toast.makeText(this, "Location permission needed. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

    private class ArretProcheLocation extends AsyncTask<String, Boolean, Boolean> {
        protected void onPreExecute() {

        }

        protected Boolean doInBackground(String... urls ) {
            callWebService(longitude,latitude, kmRayon);

            return true;
        }

        protected void onPostExecute(Integer result) {

        }


        private void callWebService(double x, double y, int km) {
            String url = "https://data.metromobilite.fr/api/linesNear/json?x="+x+"&y="+y+"&dist="+km+"&details=true";
            RequestQueue queue = Volley.newRequestQueue(ListviewActivity.this);

            StringRequest request = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            String a = response;
                            arretsMetro = new ArretsMetro(response);
                            // arretsTram = new ArretsMetro(getOnlyTram(arretsMetro).getLesArrets());
                            libellesArrets.clear();
                            swipeRefreshLayout.setRefreshing(true);

                            for (ArretsCoupleMetro arretTram: arretsMetro.getLesArrets()){
                                libellesArrets.add(arretTram);
                            }
                            swipeRefreshLayout.setRefreshing(false);
                            runOnUiThread(new Runnable() {
                                public void run() {

                                    arretsAdapter.addAll(libellesArrets);

                                }

                            });

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyError a = error;
                        }
                    });

            queue.add(request);
        }


    }

    private boolean manageNavigationViewClick(MenuItem item) {
        item.setChecked(true);
        drawerLayout.closeDrawers();


            if (item.getItemId() == R.id.menu_home) {
                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
            }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        if (item.getItemId() == android.R.id.home) {
            if (menuOpen) {
                drawerLayout.closeDrawers();
                menuOpen = false;
            } else {
                drawerLayout.openDrawer(GravityCompat.START);
                menuOpen = true;
            }
        }
        return super.onOptionsItemSelected(item);
    }


}


