package com.example.myfirstapp;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class Patterns {

    private List<Pattern> patterns;

    public List<Pattern> getPatterns() {
        return patterns;
    }

    public Patterns() {
        patterns = new ArrayList<>();

    }

    public void ajouterPatterns(String jsonString) {
        try {
            JSONArray array = new JSONArray(jsonString);

            for (int i=0; i<array.length(); i++) {
                Pattern pattern = new Pattern(array.getJSONObject(i));
                boolean dejaPresent = false;
                for(Pattern pattern1: patterns) {
                    if (pattern.toString().toLowerCase().replaceAll(" ","").equals(pattern1.toString().toLowerCase().replaceAll(" ",""))) {
                        pattern1.setTimes(pattern.getTimes());
                        dejaPresent = true;
                        break;
                    }
                }
                if (!dejaPresent) {
                    patterns.add(pattern);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
