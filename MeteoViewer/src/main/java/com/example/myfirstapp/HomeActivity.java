package com.example.myfirstapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.internal.NavigationMenu;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private NavigationView navigationMenu;
    private boolean menuOpen;

    List<ArretsCoupleMetro> libellesArrets = new ArrayList<>();
    TramAdapter favorisAdapter;
    SharedPreferences favorisArrets;
    ListView favorisListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        menuOpen = false;
        String userLogin = getIntent().getStringExtra("userLogin");
        favorisListView = findViewById(R.id.lstFav);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_home);
        favorisArrets = getSharedPreferences("favoris", MODE_PRIVATE);
        favorisAdapter = new TramAdapter(this, android.R.layout.simple_list_item_1);
        remplirFavoris(favorisArrets.getString("favoris", ""));
        favorisListView.setAdapter(favorisAdapter);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        navigationMenu = findViewById(R.id.nav_view);
        TextView welcome = findViewById(R.id.welcome);
        if (userLogin == null) {
            userLogin = "";
        }
        welcome.setText("Welcome "+userLogin);
        navigationMenu.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                return manageNavigationViewClick(menuItem);
            }
        });

        View viewHeader = getLayoutInflater().inflate(R.layout.nav_header, null);

        navigationMenu.addHeaderView(viewHeader);

        favorisListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                ArretsCoupleMetro o = (ArretsCoupleMetro)favorisListView.getItemAtPosition(position);
                Intent intent = new Intent(getBaseContext(), TramDetailActivity.class);
                intent.putExtra("idList", o.getListOfId());
                intent.putExtra("nameArret", o.getName());
                startActivity(intent);
            }
        });
    }

    private boolean manageNavigationViewClick(MenuItem item) {
        item.setChecked(true);
        drawerLayout.closeDrawers();

        if (item.getItemId() == R.id.menu_meteo) {
            Intent intent = new Intent(this, ListviewActivity.class);
            startActivity(intent);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        if (item.getItemId() == android.R.id.home) {
            if (menuOpen) {
                drawerLayout.closeDrawers();
                menuOpen = false;
            } else {
                drawerLayout.openDrawer(GravityCompat.START);
                menuOpen = true;
            }
        }



        return super.onOptionsItemSelected(item);
    }

    private void remplirFavoris(String favoris) {
        if (favoris=="") {
            return;
        }
        String[] fav = favoris.split("&");

        for (int i=0; i<fav.length; i++) {
            String[] arretIds = fav[i].split("#");

            if (arretIds.length > 1) {
                String arretName = arretIds[0];
                ArretsCoupleMetro arretsCoupleMetro = null;
                for (int j = 1; j < arretIds.length; j++) {
                    if (j==1) {
                        arretsCoupleMetro = new ArretsCoupleMetro(new ArretMetro("-,"+arretName, arretIds[j]));
                    } else {
                        arretsCoupleMetro.addArret(new ArretMetro("-,"+arretName, arretIds[j]));
                    }


                }
                if (arretsCoupleMetro!= null){
                    libellesArrets.add(arretsCoupleMetro);
                }


            }

        }
        favorisAdapter.addAll(libellesArrets);
    }

}
