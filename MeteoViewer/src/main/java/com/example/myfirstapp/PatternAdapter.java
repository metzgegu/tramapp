package com.example.myfirstapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PatternAdapter extends ArrayAdapter<Pattern> {
    Context m_context;
    List<Pattern> lstItems;

    public PatternAdapter(@NonNull Context context, int resource) {
        super(context, resource);
        m_context = context;
        lstItems = new ArrayList<>();
    }

    @Override
    public int getPosition(Pattern arretMetro) {
        return lstItems.indexOf(arretMetro);
    }

    @Override
    public int getCount() {
        return lstItems.size();
    }

    @Override
    public Pattern getItem(int position){

        return lstItems.get(position);
    }

    @Override
    public void addAll(@NonNull Collection<? extends Pattern> collection) {
        lstItems.clear();
        lstItems.addAll(collection);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.pattern_layout, parent, false);
        TextView txtName = rowView.findViewById(R.id.txtName);
        //TextView txtDirection = rowView.findViewById(R.id.txtDirection);

        Pattern pattern = getItem(position);
        String id = pattern.getId();
        String id2 ="";
        if (id!= null) {
            String[] res = id.split(":");
            id2 = res[1];
        }
        txtName.setText(id2+" | "+pattern.getDesc());

        // txtDirection.setText("Pas encore implémenté");

        return rowView;
    }
}
